package nprimenumber;

import java.util.Random;

public class NPrimeNumbers {
    public static void main(String[] args) {
        final int N = 15;
        Random random = new Random();
        int[] array = new int[N]; //массив
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        int maxIndex = -1, minIndex = -1;
        int first, last;
        int in;
        int sum = 0; //сума
        long product = 1; //добуток між мін і макс
        long pr = 1; //добуток елементів з парними числами
        int sumFirstLast = 0; //сума між першим і останнім від’ємними числами
        for (int i = 0; i < N; ++i) {
            array[i] = random.nextInt(1000) - 99; //заповнюємо масив
            System.out.print(array[i] + " "); //виводимо
            if (array[i] < 0) sum += array[i]; // якщо число від’ємне, рахуємо суму
            if ((array[i]%2) == 0) pr *= array[i]; // якщо залишок від ділення на 2 = 0, рахуємо добуток елементів з парними числами


            if (array[i] > max) { //пошук максимума і його індекса
                max = array[i];
                maxIndex = i;
            }
            if (array[i] < min) { //пошук мінімума і його індекса
                min = array[i];
                minIndex = i;
            }
        }
        System.out.println();

        for(first = 0; first < 15; first++ ) {//прохід з початку до кінця масиву
            if( array[first] < 0 ) break;
        }
        for( last = 14; last > first; last-- ) {//прохід з кінця до початку масиву
            if( array[last] < 0 ) break;
        }
        for( in = first+1; in < last; in++ ) {
            sumFirstLast += array[in];//рахуємо суму між першим і останнім від’ємними елементами
        }

        if (maxIndex > minIndex) { //якщо треба міняємо місцями індекси, щоб першим був меньший
            maxIndex = maxIndex + minIndex;
            minIndex = maxIndex - minIndex;
            maxIndex = maxIndex - minIndex;
        }
        for (int i = maxIndex + 1; i < minIndex; ++i) { //від меньшого індекса до більшого
            product *= array[i]; //рахуємо добуток
        }

        System.out.println("Сума від’ємних чисел: " + sum);
        System.out.println("Мінімальный елемент: " + min);
        System.out.println("Максимальний елемент: " + max);
        System.out.println("Добуток елементів між максимальним і мінімальним: " + product);
        System.out.println("Добуток елементів із парними числами: " + pr);
        System.out.println("Сума елементів між першим і останнім від’ємними числами: " + sumFirstLast);
    }
}

