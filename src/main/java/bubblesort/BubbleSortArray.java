package bubblesort;

public class BubbleSortArray {
    static void bubbleSort(int[] sort, int n) {
        int i, j, temp;
        boolean swapped;
        for (i = 0; i < n - 1; i++) {
            swapped = false;
            for (j = 0; j < n - i - 1; j++) {
                if (sort[j] > sort[j + 1]) {
                    temp = sort[j];
                    sort[j] = sort[j + 1];
                    sort[j + 1] = temp;
                    swapped = true;
                }
            }

            if (swapped == false)
                break;
        }
    }

    static void printArray(int[] sort, int size) {
        int i;
        for (i = 0; i < size; i++)
            System.out.print(sort[i] + " ");
        System.out.println();
    }

    public static void main(String[] args) {
        int[] sort = {64, -3, -18, 0, 22, 11, 90};
        int n = sort.length;
        bubbleSort(sort, n);
        System.out.println("Масив, відсортований методом бульбашок: ");
        printArray(sort, n);
    }
}
