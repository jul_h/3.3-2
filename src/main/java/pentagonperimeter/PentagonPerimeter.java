package pentagonperimeter;

import java.util.Arrays;
import java.util.Scanner;

public class PentagonPerimeter {

    public static void main(String args[]){
        System.out.println("Кількість сторін: ");
        Scanner s = new Scanner(System.in);
        int size = s.nextInt();
        int[] sides = new int [size];
        int perimeter = 0;
        System.out.println("Введіть довжину сторони: ");

        for(int i=0; i<size; i++){
            sides[i] = s.nextInt();
            perimeter = perimeter + sides[i];
        }
        System.out.println("Сторони п’ятикутника: " + Arrays.toString(sides));
        System.out.println("Периметр п’ятикутника: " + perimeter);
    }
}
