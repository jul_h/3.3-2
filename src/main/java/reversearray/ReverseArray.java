package reversearray;

public class ReverseArray {
    static void reverseArray(int[] nums, int start, int end) {
        int temp;

        while (start < end) {
            temp = nums[start];
            nums[start] = nums[end];
            nums[end] = temp;
            start++;
            end--;
        }
    }

    static void printArray(int[] nums, int size) {
        for (int i = 0; i < size; i++)
            System.out.print(nums[i] + " ");

        System.out.println();
    }

    public static void main(String[] args) {

        int[] nums = {3, 2, 1, 6, 5, 4};
        printArray(nums, 6);
        reverseArray(nums, 0, 5);
        System.out.print("Одновимірний масив у зворотному порядку \n");
        printArray(nums, 6);

    }
}
