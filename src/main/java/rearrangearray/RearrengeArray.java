package rearrangearray;

import java.util.Arrays;
import java.util.Random;

public class RearrengeArray {

    static void rand(int[] array, int a) {
        Random r = new Random();

        for (int i = a - 1; i > 0; i--) {
            int j = r.nextInt(i + 1);

            int temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        System.out.println("Перемішані елементи масиву: \n" + Arrays.toString(array));
    }

    public static void main(String[] args) {

        int[] ar = {1, 2, 3, 4, 5, 6, 7, 8};
        int b = ar.length;
        rand (ar, b);
    }
}
