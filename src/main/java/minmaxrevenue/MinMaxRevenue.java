package minmaxrevenue;

import java.util.Arrays;
import java.util.Scanner;

public class MinMaxRevenue {
    public int max(int [] array) {
        int max = 0;

        for(int i=0; i<array.length; i++ ) {
            if(array[i]>max) {
                max = array[i];
            }
        }
        return max;
    }
    public int min(int [] array) {
        int min = array[0];

        for(int i=0; i<array.length; i++ ) {
            if(array[i]<min) {
                min = array[i];
            }
        }
        return min;
    }
    public static void main(String args[]) {

        System.out.println("Введіть прибуток: ");
        Scanner s = new Scanner(System.in);

        int[] revenue = new int [12];

        for(int i=0; i<12; i++) {

            revenue[i] = s.nextInt();

        }
        System.out.println("Прибуток за 12 місяців: " + Arrays.toString(revenue));

        MinMaxRevenue m = new MinMaxRevenue();
        System.out.println("Максимальний прибуток: " + m.max(revenue));
        System.out.println("Мінімальний прибуток: " + m.min(revenue));
    }
}
